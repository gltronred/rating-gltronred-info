#!/bin/bash

BASE="http://rating.chgk.info/api"
BAS="http://rating.chgk.info"

# список игроков
echo -ne "Downloading players "
rm players.json
pages=$(curl -s -X GET "$BASE/players.json" | jq -r '.total_items' | sed 's/[0-9]\{3\}$//g;')
for i in `seq 1 $(($pages + 1))`; do
    echo -ne .
    curl -s -X GET "$BASE/players.json?page=$i" | jq -c '.items[]' >> players.json
done
echo

# список команд
echo -ne "Downloading teams "
pages=$(curl -s -X GET "$BASE/teams.json" | jq -r '.total_items' | sed 's/[0-9]\{3\}$//g;')
rm teams.json
for i in `seq 1 $(($pages + 1))`; do
    echo -ne .
    curl -s -X GET "$BASE/teams.json?page=$i" | jq -c '.items[]' >> teams.json
done
echo

# список турниров
echo -ne "Downloading tournaments "
rm tournaments.json
pages=$(curl -s -X GET "$BASE/tournaments.json" | jq -r '.total_items' | sed 's/[0-9]\{3\}$//g;')
for i in `seq 1 $(($pages + 1))`; do
    curl -s -X GET "$BASE/tournaments.json?page=$i" | jq -cr '.items[]' >> tournaments.json
done
echo

# БС команд
team_ids=$(cat teams.json | jq -sr 'map(.idteam | tonumber) | sort_by(.)[]')
max_team=$(cat teams.json | jq -s 'map(.idteam | tonumber) | max')
echo -ne "Downloading team recaps\n"
for i in $team_ids; do
    echo -ne "$i / $max_team\r"
    curl -s -X GET "$BASE/teams/$i/recaps.json" > base-recap-$i.json
done
echo

# составы команд на турнире
# результаты команд на турнире
tournament_ids=$(cat tournaments.json | jq -sr 'map(.idtournament | tonumber) | sort_by(.)[]')
max_tournament=$(cat tournaments.json | jq -sr 'map(.idtournament | tonumber) | max')
echo -ne "Downloading tournament recaps and results\n"
for i in $tournament_ids; do
    curl -s -X GET "$BASE/tournaments/${i}/list.json" > tournament-$i-list.json
    #curl -s -X GET "$BAS/tournaments.php?tournament_id=${i}&download_data=export_tournament_tour" > tournament-$i-result.csv
    curl -s -X GET "$BAS/tournaments.php?tournament_id=${i}&download_data=export_tournament_with_players" > tournament-$i-players.csv
    echo -ne "$i / $max_tournament\r"
done
echo

