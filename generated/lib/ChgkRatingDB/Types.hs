{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -fno-warn-unused-binds -fno-warn-unused-imports #-}

module ChgkRatingDB.Types (
  Change (..),
  DatetimeRange (..),
  Error (..),
  Inline_response_200 (..),
  Inline_response_200_1 (..),
  Inline_response_200_1_list (..),
  Inline_response_200_2 (..),
  Inline_response_200_3 (..),
  Inline_response_200_4 (..),
  Player (..),
  Recap (..),
  Recap_inner (..),
  ResultList (..),
  ResultList_inner (..),
  Team (..),
  TeamTournament (..),
  Tournament (..),
  Tournament_dates (..),
  Tournament_payment (..),
  Update (..),
  UpdateData (..),
  UpdateData_appeal (..),
  UpdateData_controversial (..),
  UpdateData_disqualify (..),
  ) where

import Data.List (stripPrefix)
import Data.Maybe (fromMaybe)
import Data.Aeson (Value, FromJSON(..), ToJSON(..), genericToJSON, genericParseJSON)
import Data.Aeson.Types (Options(..), defaultOptions)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Map as Map
import GHC.Generics (Generic)
import Data.Function ((&))


-- | Изменение в таблице турнира &#x60;tournament-id&#x60;.   При задании обоих параметров &#x60;team-id&#x60; и &#x60;question&#x60; в таблице выбирается одна ячейка. Если один из этих параметров не задан -- выбирается вся соответствующая строка (когда задан &#x60;team-id&#x60;) или столбец (когда задан &#x60;question&#x60;).  Параметр &#x60;value&#x60; устанавливает новое значение в выбранном диапазоне -- есть ли \&quot;плюсик\&quot; в каждой ячейке выбранного диапазона.
data Change = Change
  { changeTournament'Dashid :: Int -- ^ В каком турнире
  , changeTeam'Dashid :: Int -- ^ Какой команде
  , changeQuestion :: Int -- ^ На какой вопрос
  , changeValue :: Bool -- ^ Зачли/не зачли что-то
  } deriving (Show, Eq, Generic)

instance FromJSON Change where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "change")
instance ToJSON Change where
  toJSON = genericToJSON (removeFieldLabelPrefix False "change")

-- | Диапазон дат (точнее, даты-времени)
data DatetimeRange = DatetimeRange
  { datetimeRangeFrom :: Integer -- ^ Дата-время начала
  , datetimeRangeTo :: Integer -- ^ Дата-время окончания
  } deriving (Show, Eq, Generic)

instance FromJSON DatetimeRange where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "datetimeRange")
instance ToJSON DatetimeRange where
  toJSON = genericToJSON (removeFieldLabelPrefix False "datetimeRange")

-- | This general error structure is used throughout this API.
data Error = Error
  { errorCode :: Int -- ^ 
  , errorDescription :: Text -- ^ 
  , errorReasonPhrase :: Text -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON Error where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "error")
instance ToJSON Error where
  toJSON = genericToJSON (removeFieldLabelPrefix False "error")

-- | 
data Inline_response_200 = Inline_response_200
  { inlineResponse200Total :: Int -- ^ Общее количество игроков
  , inlineResponse200List :: [Player] -- ^ Список игроков
  } deriving (Show, Eq, Generic)

instance FromJSON Inline_response_200 where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "inlineResponse200")
instance ToJSON Inline_response_200 where
  toJSON = genericToJSON (removeFieldLabelPrefix False "inlineResponse200")

-- | 
data Inline_response_200_1 = Inline_response_200_1
  { inlineResponse2001Total :: Int -- ^ Общее количество команд
  , inlineResponse2001List :: [Inline_response_200_1_list] -- ^ Команды
  } deriving (Show, Eq, Generic)

instance FromJSON Inline_response_200_1 where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "inlineResponse2001")
instance ToJSON Inline_response_200_1 where
  toJSON = genericToJSON (removeFieldLabelPrefix False "inlineResponse2001")

-- | 
data Inline_response_200_1_list = Inline_response_200_1_list
  { inlineResponse2001ListTeam :: Team -- ^ 
  , inlineResponse2001ListBase'Dashrecap :: Recap -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON Inline_response_200_1_list where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "inlineResponse2001List")
instance ToJSON Inline_response_200_1_list where
  toJSON = genericToJSON (removeFieldLabelPrefix False "inlineResponse2001List")

-- | 
data Inline_response_200_2 = Inline_response_200_2
  { inlineResponse2002Total :: Int -- ^ Количество команд в турнире
  , inlineResponse2002List :: [TeamTournament] -- ^ Список результатов команд
  } deriving (Show, Eq, Generic)

instance FromJSON Inline_response_200_2 where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "inlineResponse2002")
instance ToJSON Inline_response_200_2 where
  toJSON = genericToJSON (removeFieldLabelPrefix False "inlineResponse2002")

-- | 
data Inline_response_200_3 = Inline_response_200_3
  { inlineResponse2003Team'Dashid :: Int -- ^ Идентификатор команды
  , inlineResponse2003Value :: Bool -- ^ Взят ли командой вопрос
  } deriving (Show, Eq, Generic)

instance FromJSON Inline_response_200_3 where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "inlineResponse2003")
instance ToJSON Inline_response_200_3 where
  toJSON = genericToJSON (removeFieldLabelPrefix False "inlineResponse2003")

-- | 
data Inline_response_200_4 = Inline_response_200_4
  { inlineResponse2004Last'Dashupdate :: Int -- ^ Время последнего на данный момент обновления
  , inlineResponse2004List :: [Update] -- ^ Обновления
  } deriving (Show, Eq, Generic)

instance FromJSON Inline_response_200_4 where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "inlineResponse2004")
instance ToJSON Inline_response_200_4 where
  toJSON = genericToJSON (removeFieldLabelPrefix False "inlineResponse2004")

-- | Информация об игроке  Поскольку хорошей системы для хранения имен не существует [1][2], в данном API используется имеющаяся на сайте рейтинга.  [1] https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/  [2] https://www.w3.org/International/questions/qa-personal-names
data Player = Player
  { playerPlayer'Dashid :: Text -- ^ Идентификатор игрока
  , playerSurname :: Text -- ^ Фамилия
  , playerName :: Text -- ^ Имя (может быть пустым, см. например, 164387, Чоо Хи Чен)
  , playerPatronymic :: Text -- ^ Отчество (может быть пустым, например, Шень Александр)
  } deriving (Show, Eq, Generic)

instance FromJSON Player where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "player")
instance ToJSON Player where
  toJSON = genericToJSON (removeFieldLabelPrefix False "player")

-- | Состав команды
newtype Recap = Recap { unRecap :: [Recap_inner] }
  deriving (Show, Eq, FromJSON, ToJSON, Generic)

-- | 
data Recap_inner = Recap_inner
  { recapInnerPlayer'Dashid :: Int -- ^ Идентификатор игрока
  , recapInnerCategory :: Text -- ^ Базовый/капитан/легионер
  } deriving (Show, Eq, Generic)

instance FromJSON Recap_inner where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "recapInner")
instance ToJSON Recap_inner where
  toJSON = genericToJSON (removeFieldLabelPrefix False "recapInner")

-- | 
newtype ResultList = ResultList { unResultList :: [ResultList_inner] }
  deriving (Show, Eq, FromJSON, ToJSON, Generic)

-- | 
data ResultList_inner = ResultList_inner
  { resultListInnerQuestion :: Int -- ^ Номер вопроса
  , resultListInnerValue :: Bool -- ^ Взят ли вопрос
  } deriving (Show, Eq, Generic)

instance FromJSON ResultList_inner where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "resultListInner")
instance ToJSON ResultList_inner where
  toJSON = genericToJSON (removeFieldLabelPrefix False "resultListInner")

-- | Команда
data Team = Team
  { teamTeam'Dashid :: Int -- ^ Идентификатор команды
  , teamName :: Text -- ^ Название команды
  , teamTown :: Text -- ^ Город приписки
  } deriving (Show, Eq, Generic)

instance FromJSON Team where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "team")
instance ToJSON Team where
  toJSON = genericToJSON (removeFieldLabelPrefix False "team")

-- | 
data TeamTournament = TeamTournament
  { teamTournamentTeam'Dashid :: Int -- ^ Идентификатор команды
  , teamTournamentRecap :: Recap -- ^ 
  , teamTournamentResults :: ResultList -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON TeamTournament where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "teamTournament")
instance ToJSON TeamTournament where
  toJSON = genericToJSON (removeFieldLabelPrefix False "teamTournament")

-- | Данные о турнире
data Tournament = Tournament
  { tournamentTournament'Dashid :: Int -- ^ Идентификатор турнира
  , tournamentName :: Text -- ^ Название турнира
  , tournamentOrganizers :: [Player] -- ^ Оргкомитет
  , tournamentGame'Dashjury :: [Player] -- ^ Игровое жюри
  , tournamentAppeal'Dashjury :: [Player] -- ^ Апелляционное жюри
  , tournamentTour'Dashformula :: [Int] -- ^ Количество вопросов по турам
  , tournamentHas'Dashrating :: Bool -- ^ Рейтингуется ли турнир?
  , tournamentTournament'Dashtype :: Text -- ^ Тип турнира
  , tournamentPayment :: [Tournament_payment] -- ^ Информация об оплате
  , tournamentDates :: Tournament_dates -- ^ 
  , tournamentExtra'Dashinfo :: Text -- ^ Дополнительная информация о турнире
  } deriving (Show, Eq, Generic)

instance FromJSON Tournament where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "tournament")
instance ToJSON Tournament where
  toJSON = genericToJSON (removeFieldLabelPrefix False "tournament")

-- | Даты
data Tournament_dates = Tournament_dates
  { tournamentDatesRequests'Dashallowed'Dashbefore :: Integer -- ^ Дата и время, до которого возможна подача заявок
  , tournamentDatesDownloading'Dashquestions :: DatetimeRange -- ^ 
  , tournamentDatesReceiving'Dashrecaps :: DatetimeRange -- ^ 
  , tournamentDatesReceiving'Dashresults :: DatetimeRange -- ^ 
  , tournamentDatesReceiving'Dashappeals :: DatetimeRange -- ^ 
  , tournamentDatesControversial'Dashresults :: Integer -- ^ Завершение рассмотрения спорных
  , tournamentDatesAppeal'Dashresults :: Integer -- ^ Завершение рассмотрения апелляций
  , tournamentDatesPayment :: Text -- ^ Время, до которого необходима оплата
  } deriving (Show, Eq, Generic)

instance FromJSON Tournament_dates where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "tournamentDates")
instance ToJSON Tournament_dates where
  toJSON = genericToJSON (removeFieldLabelPrefix False "tournamentDates")

-- | 
data Tournament_payment = Tournament_payment
  { tournamentPaymentSize :: Text -- ^ Размер оплаты
  , tournamentPaymentCategory :: Text -- ^ Категория участников с такой оплатой
  } deriving (Show, Eq, Generic)

instance FromJSON Tournament_payment where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "tournamentPayment")
instance ToJSON Tournament_payment where
  toJSON = genericToJSON (removeFieldLabelPrefix False "tournamentPayment")

-- | 
data Update = Update
  { updateTimestamp :: Int -- ^ Время обновления в миллисекундах с начала эпохи
  , updateData :: UpdateData -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON Update where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "update")
instance ToJSON Update where
  toJSON = genericToJSON (removeFieldLabelPrefix False "update")

-- | Информация об обновлении
data UpdateData = UpdateData
  { updateDataUpdate'Dashmeta :: Tournament -- ^ 
  , updateDataInput :: [Change] -- ^ 
  , updateDataControversial :: UpdateData_controversial -- ^ 
  , updateDataAppeal :: UpdateData_appeal -- ^ 
  , updateDataDisqualify :: UpdateData_disqualify -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON UpdateData where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "updateData")
instance ToJSON UpdateData where
  toJSON = genericToJSON (removeFieldLabelPrefix False "updateData")

-- | Засчитанная апелляция
data UpdateData_appeal = UpdateData_appeal
  { updateDataAppealInput :: [Change] -- ^ Затронутые вопросы и команды
  , updateDataAppealText :: Text -- ^ Апелляция
  , updateDataAppealVerdict :: Text -- ^ Вердикт АЖ
  } deriving (Show, Eq, Generic)

instance FromJSON UpdateData_appeal where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "updateDataAppeal")
instance ToJSON UpdateData_appeal where
  toJSON = genericToJSON (removeFieldLabelPrefix False "updateDataAppeal")

-- | Засчитанный спорный
data UpdateData_controversial = UpdateData_controversial
  { updateDataControversialInput :: [Change] -- ^ Список затронутых вопросов и команд
  , updateDataControversialText :: Text -- ^ Текст спорного
  , updateDataControversialVerdict :: Text -- ^ Текст от ИЖ
  } deriving (Show, Eq, Generic)

instance FromJSON UpdateData_controversial where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "updateDataControversial")
instance ToJSON UpdateData_controversial where
  toJSON = genericToJSON (removeFieldLabelPrefix False "updateDataControversial")

-- | 
data UpdateData_disqualify = UpdateData_disqualify
  { updateDataDisqualifyInput :: [Change] -- ^ Затронутые команды
  , updateDataDisqualifyVerdict :: Text -- ^ Текст решения
  } deriving (Show, Eq, Generic)

instance FromJSON UpdateData_disqualify where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "updateDataDisqualify")
instance ToJSON UpdateData_disqualify where
  toJSON = genericToJSON (removeFieldLabelPrefix False "updateDataDisqualify")

-- Remove a field label prefix during JSON parsing.
-- Also perform any replacements for special characters.
removeFieldLabelPrefix :: Bool -> String -> Options
removeFieldLabelPrefix forParsing prefix =
  defaultOptions
  {fieldLabelModifier = fromMaybe (error ("did not find prefix " ++ prefix)) . stripPrefix prefix . replaceSpecialChars}
  where
    replaceSpecialChars field = foldl (&) field (map mkCharReplacement specialChars)
    specialChars =
      [ ("@", "'At")
      , ("\\", "'Back_Slash")
      , ("<=", "'Less_Than_Or_Equal_To")
      , ("\"", "'Double_Quote")
      , ("[", "'Left_Square_Bracket")
      , ("]", "'Right_Square_Bracket")
      , ("^", "'Caret")
      , ("_", "'Underscore")
      , ("`", "'Backtick")
      , ("!", "'Exclamation")
      , ("#", "'Hash")
      , ("$", "'Dollar")
      , ("%", "'Percent")
      , ("&", "'Ampersand")
      , ("'", "'Quote")
      , ("(", "'Left_Parenthesis")
      , (")", "'Right_Parenthesis")
      , ("*", "'Star")
      , ("+", "'Plus")
      , (",", "'Comma")
      , ("-", "'Dash")
      , (".", "'Period")
      , ("/", "'Slash")
      , (":", "'Colon")
      , ("{", "'Left_Curly_Bracket")
      , ("|", "'Pipe")
      , ("<", "'LessThan")
      , ("!=", "'Not_Equal")
      , ("=", "'Equal")
      , ("}", "'Right_Curly_Bracket")
      , (">", "'GreaterThan")
      , ("~", "'Tilde")
      , ("?", "'Question_Mark")
      , (">=", "'Greater_Than_Or_Equal_To")
      ]
    mkCharReplacement (replaceStr, searchStr) = T.unpack . replacer (T.pack searchStr) (T.pack replaceStr) . T.pack
    replacer =
      if forParsing
        then flip T.replace
        else T.replace
