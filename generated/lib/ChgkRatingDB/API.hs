{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC
-fno-warn-unused-binds -fno-warn-unused-imports -fcontext-stack=328 #-}

module ChgkRatingDB.API
  -- * Client and Server
  ( ServerConfig(..)
  , ChgkRatingDBBackend(..)
  , runChgkRatingDBServer
  -- ** Servant
  , ChgkRatingDBAPI
  ) where

import ChgkRatingDB.Types

import Control.Monad.Except (ExceptT)
import Control.Monad.IO.Class
import Data.Aeson (Value)
import Data.Coerce (coerce)
import Data.Function ((&))
import qualified Data.Map as Map
import Data.Monoid ((<>))
import Data.Proxy (Proxy(..))
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Exts (IsString(..))
import GHC.Generics (Generic)
import Network.HTTP.Client (Manager, defaultManagerSettings, newManager)
import Network.HTTP.Types.Method (methodOptions)
import qualified Network.Wai.Handler.Warp as Warp
import Servant (Handler, ServantErr, serve)
import Servant.API
import Servant.API.Verbs (StdMethod(..), Verb)
import Servant.Client (Scheme(Http), ServantError, client)
import Web.HttpApiData




-- For the form data code generation.
lookupEither :: FromHttpApiData b => Text -> [(Text, Text)] -> Either String b
lookupEither key assocs =
  case lookup key assocs of
    Nothing -> Left $ "Could not find parameter " <> (T.unpack key) <> " in form data"
    Just value ->
      case parseQueryParam value of
        Left result -> Left $ T.unpack result
        Right result -> Right $ result

-- | Servant type-level API, generated from the Swagger spec for ChgkRatingDB.
type ChgkRatingDBAPI
    =    "players" :> QueryParam "from" Int :> Verb 'GET 200 '[JSON] Inline_response_200 -- 'playersGet' route
    :<|> "players" :> Capture "player-id" Text :> Verb 'DELETE 200 '[JSON] () -- 'playersPlayerIdDelete' route
    :<|> "players" :> Capture "player-id" Text :> Verb 'GET 200 '[JSON] Player -- 'playersPlayerIdGet' route
    :<|> "players" :> Capture "player-id" Text :> ReqBody '[JSON] Player :> Verb 'PUT 200 '[JSON] Player -- 'playersPlayerIdPut' route
    :<|> "players" :> ReqBody '[JSON] Player :> Verb 'POST 200 '[JSON] Player -- 'playersPost' route
    :<|> "teams" :> QueryParam "from" Int :> Verb 'GET 200 '[JSON] Inline_response_200_1 -- 'teamsGet' route
    :<|> "teams" :> ReqBody '[JSON] Team :> Verb 'POST 200 '[JSON] () -- 'teamsPost' route
    :<|> "tournaments" :> QueryParam "release" Text :> Verb 'GET 200 '[JSON] [Tournament] -- 'tournamentsGet' route
    :<|> "tournaments" :> Capture "tournament-id" Int :> QueryParam "from" Int :> Verb 'GET 200 '[JSON] Inline_response_200_2 -- 'tournamentsTournamentIdGet' route
    :<|> "tournaments" :> Capture "tournament-id" Int :> "questions" :> Capture "question" Int :> Verb 'GET 200 '[JSON] [Inline_response_200_3] -- 'tournamentsTournamentIdQuestionsQuestionGet' route
    :<|> "tournaments" :> Capture "tournament-id" Int :> "teams" :> Capture "team-id" Int :> Verb 'GET 200 '[JSON] TeamTournament -- 'tournamentsTournamentIdTeamsTeamIdGet' route
    :<|> "updates" :> QueryParam "since" Integer :> Verb 'GET 200 '[JSON] Inline_response_200_4 -- 'updatesGet' route
    :<|> "updates" :> ReqBody '[JSON] UpdateData :> Verb 'POST 200 '[JSON] () -- 'updatesPost' route

-- | Server or client configuration, specifying the host and port to query or serve on.
data ServerConfig = ServerConfig
  { configHost :: String  -- ^ Hostname to serve on, e.g. "127.0.0.1"
  , configPort :: Int      -- ^ Port to serve on, e.g. 8080
  } deriving (Eq, Ord, Show, Read)

-- | Backend for ChgkRatingDB.
-- The backend can be used both for the client and the server. The client generated from the ChgkRatingDB Swagger spec
-- is a backend that executes actions by sending HTTP requests (see @createChgkRatingDBClient@). Alternatively, provided
-- a backend, the API can be served using @runChgkRatingDBServer@.
data ChgkRatingDBBackend m = ChgkRatingDBBackend
  { playersGet :: Maybe Int -> m Inline_response_200{- ^  -}
  , playersPlayerIdDelete :: Text -> m (){- ^  -}
  , playersPlayerIdGet :: Text -> m Player{- ^  -}
  , playersPlayerIdPut :: Text -> Player -> m Player{- ^ Пользователь должен быть авторизован для выполнения этой операции с данным игроком -}
  , playersPost :: Player -> m Player{- ^ Проверка, не существует ли уже такой игрок, не являются ли данные уточнением существующего игрока, ложится на пользователя API -}
  , teamsGet :: Maybe Int -> m Inline_response_200_1{- ^  -}
  , teamsPost :: Team -> m (){- ^ Проверку, что такая команда не существует, должен выполнять пользователь API -}
  , tournamentsGet :: Maybe Text -> m [Tournament]{- ^ * Возвращаются турниры, впервые учтённые в релизе YYYY-MM-DD. * Для релиза stable возвращаются все завершённые турниры. * Для релиза unstable возвращаются все незавершённые турниры. * Для релиза all возвращаются все турниры.  Ввиду технических сложностей, в версии 0.1 работает только значение параметра all; кроме того, в структуре [Tournament] заполнены только `tournament-id` и `name`. -}
  , tournamentsTournamentIdGet :: Int -> Maybe Int -> m Inline_response_200_2{- ^ Результаты получаются блоками по тысяче команд -}
  , tournamentsTournamentIdQuestionsQuestionGet :: Int -> Int -> m [Inline_response_200_3]{- ^ Эти данные, конечно, можно получить из общей таблицы, но запрос кажется удобным -}
  , tournamentsTournamentIdTeamsTeamIdGet :: Int -> Int -> m TeamTournament{- ^ Внимание: из-за апелляций некоторые вопросы могут быть сняты, поэтому номера в списке могут быть с пропусками -}
  , updatesGet :: Maybe Integer -> m Inline_response_200_4{- ^  -}
  , updatesPost :: UpdateData -> m (){- ^  -}
  }

-- | Run the ChgkRatingDB server at the provided host and port.
runChgkRatingDBServer :: MonadIO m => ServerConfig -> ChgkRatingDBBackend Handler  -> m ()
runChgkRatingDBServer ServerConfig{..} backend =
  liftIO $ Warp.runSettings warpSettings $ serve (Proxy :: Proxy ChgkRatingDBAPI) (serverFromBackend backend)
  where
    warpSettings = Warp.defaultSettings & Warp.setPort configPort & Warp.setHost (fromString configHost)
    serverFromBackend ChgkRatingDBBackend{..} =
      (playersGet :<|>
       playersPlayerIdDelete :<|>
       playersPlayerIdGet :<|>
       playersPlayerIdPut :<|>
       playersPost :<|>
       teamsGet :<|>
       teamsPost :<|>
       tournamentsGet :<|>
       tournamentsTournamentIdGet :<|>
       tournamentsTournamentIdQuestionsQuestionGet :<|>
       tournamentsTournamentIdTeamsTeamIdGet :<|>
       updatesGet :<|>
       updatesPost)
