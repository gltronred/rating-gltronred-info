{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module ChgkRatingDB.Backend where

import           ChgkRatingDB.Types
import           ChgkRatingDB.API (ChgkRatingDBBackend(ChgkRatingDBBackend))
import qualified ChgkRatingDB.API as API

import           Control.Lens
import           Control.Monad.Except (throwError)
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Aeson.Lens
import           Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy.Char8 as B
import           Data.Function (on)
import           Data.List
import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as ET
import           Servant.Server

import           Codec.Text.IConv

server :: ChgkRatingDBBackend Handler
server = ChgkRatingDBBackend
  { API.playersGet = playersGet
  , API.playersPlayerIdDelete = playersPlayerIdDelete
  , API.playersPlayerIdGet = playersPlayerIdGet
  , API.playersPlayerIdPut = playersPlayerIdPut
  , API.playersPost = playersPost
  , API.teamsGet = teamsGet
  , API.teamsPost = teamsPost
  , API.tournamentsGet = tournamentsGet
  , API.tournamentsTournamentIdGet = tournamentsTournamentIdGet
  , API.tournamentsTournamentIdQuestionsQuestionGet = tournamentsTournamentIdQuestionsQuestionGet
  , API.tournamentsTournamentIdTeamsTeamIdGet = tournamentsTournamentIdTeamsTeamIdGet
  , API.updatesGet = updatesGet
  , API.updatesPost = updatesPost
  }

notImplemented :: Handler ()
notImplemented = throwError $ err501 { errBody = "Method is not implemented"}

playersDB :: IO [Player]
playersDB = mapMaybe ((>>= convertPlayer) . decode) . B.lines <$> B.readFile "../tmp/data/players.json"

convertPlayer :: Value -> Maybe Player
convertPlayer obj = Just $ Player
  { playerPlayer'Dashid = obj ^. key "idplayer" . _String
  , playerSurname = obj ^. key "surname" . _String
  , playerName = obj ^. key "name" . _String
  , playerPatronymic = obj ^. key "patronymic" . _String
  }

teamsDB :: IO [Team]
teamsDB = mapMaybe ((>>= convertTeam) . decode) . B.lines <$> B.readFile "../tmp/data/teams.json"

convertTeam :: Value -> Maybe Team
convertTeam obj = do
  teamTeam'Dashid <- obj ^? key "idteam" . _String . to (read . T.unpack)
  teamName <- obj ^? key "name" . _String
  teamTown <- obj ^? key "town" . _String
  pure $ Team {..}

teamRecapDB :: IO [Inline_response_200_1_list]
teamRecapDB = map (\team -> Inline_response_200_1_list team $ Recap []) <$> teamsDB

tournamentsDB :: IO [Tournament]
tournamentsDB = mapMaybe ((>>= convertTournament) . decode) . B.lines <$> B.readFile "../tmp/data/tournaments.json"

convertTournament :: Value -> Maybe Tournament
convertTournament obj = do
  tournamentTournament'Dashid <- obj ^? key "idtournament" . _String . to (read . T.unpack)
  tournamentName <- obj ^? key "name" . _String
  tournamentTournament'Dashtype <- obj ^? key "type_name" . _String
  let tournamentOrganizers = []
      tournamentGame'Dashjury = []
      tournamentAppeal'Dashjury = []
      tournamentTour'Dashformula = []
      tournamentHas'Dashrating = True
      tournamentPayment = []
      future = 2^(40 :: Integer)
      fullRange = DatetimeRange 0 future
      tournamentDates = Tournament_dates future fullRange fullRange fullRange fullRange future future ""
      tournamentExtra'Dashinfo = ""
  pure $ Tournament {..}

tournDB :: Int -> IO [TeamTournament]
tournDB ident = do
  recaps <- sortBy (compare`on`fst) . convertRecap . map convertCsvPlayer . tail . B.lines <$>
            B.readFile ("../tmp/data/tournament-" ++ show ident ++ "-players.csv")
  results <- sortBy (compare`on`fst) . mapMaybe convertResult . fromMaybe [] . decode <$>
             B.readFile ("../tmp/data/tournament-" ++ show ident ++ "-list.json")
  pure $ mergeWith TeamTournament recaps results

mergeWith :: Ord k => (k -> a -> b -> c) -> [(k,a)] -> [(k,b)] -> [c]
mergeWith _ _ [] = []
mergeWith _ [] (_:_) = []
mergeWith f fullA@((k1,a):as) fullB@((k2,b):bs)
  | k1 == k2 = f k1 a b : mergeWith f as bs
  | k1 <  k2 = mergeWith f as fullB
  | k1 >  k2 = mergeWith f fullA bs

convertCsvPlayer :: ByteString -> (Int, Int, ByteString)
convertCsvPlayer line = let
  -- [_place, teamIdStr, _teamName, _teamTown, cat, playerIdStr, _surname, _name, _patr] = B.split ';' line
  fields = B.split ';' line
  teamIdStr = fields!!1
  cat = fields!!4
  playerIdStr = fields!!5
  teamId = read $ B.unpack teamIdStr
  playerId = read $ B.unpack playerIdStr
  in (teamId, playerId, cat)

convertRecap :: [(Int, Int, ByteString)] -> [(Int, Recap)]
convertRecap = map (toRecap . map (\(a,b,c) -> (a,Recap_inner b $ toUtf8 c))) . groupBy ((==) `on` (\(a,_,_) -> a))
  where toUtf8 = ET.decodeUtf8 . B.toStrict . convert "CP1251" "UTF-8"
        toRecap ps = (fst $ head ps, Recap $ map snd ps)

convertResult :: Value -> Maybe (Int, ResultList)
convertResult obj = do
  results <- obj ^? key "mask" . _String . to (map (uncurry ResultList_inner) . zip [1..] . map (=='1') . T.unpack)
  teamId <- obj ^? key "idteam" . _String . to (read . T.unpack)
  pure (teamId, ResultList results)

withFrom :: IO [a] -> (a -> Int) -> (Int -> [a] -> b) -> Maybe Int -> Handler b
withFrom db ident resp mfrom = do
  let from = fromMaybe 0 mfrom
  items <- liftIO db
  let count = length items
      output = take 1000 $ dropWhile ((<=from) . ident) items
  pure $ resp count output

playersGet :: Maybe Int -> Handler Inline_response_200
playersGet = withFrom playersDB (read . T.unpack . playerPlayer'Dashid) Inline_response_200

playersPost :: Player -> Handler Player
playersPost player = notImplemented >> pure player

playersPlayerIdDelete :: Text -> Handler ()
playersPlayerIdDelete = const notImplemented

playersPlayerIdPut :: Text -> Player -> Handler Player
playersPlayerIdPut ident player = notImplemented >> pure player

playersPlayerIdGet :: Text -> Handler Player
playersPlayerIdGet ident = do
  players <- liftIO playersDB
  let mplayer = find ((==ident) . playerPlayer'Dashid) players
  maybe (throwError $ err404 { errBody = "Player not found" }) pure mplayer

teamsGet :: Maybe Int -> Handler Inline_response_200_1
teamsGet = withFrom teamRecapDB (teamTeam'Dashid . inlineResponse2001ListTeam) Inline_response_200_1

teamsPost :: Team -> Handler ()
teamsPost = const notImplemented

tournamentsGet :: Maybe Text -> Handler [Tournament]
tournamentsGet Nothing = tournamentsGet $ Just "all"
tournamentsGet (Just "all") = liftIO tournamentsDB
tournamentsGet _ = notImplemented >> pure []

tournamentsTournamentIdGet :: Int -> Maybe Int -> Handler Inline_response_200_2
tournamentsTournamentIdGet ident = withFrom (liftIO $ tournDB ident) teamTournamentTeam'Dashid Inline_response_200_2

tournamentsTournamentIdTeamsTeamIdGet :: Int -> Int -> Handler TeamTournament
tournamentsTournamentIdTeamsTeamIdGet ident teamId = do
  tts <- liftIO $ tournDB ident
  maybe (throwError $ err404 { errBody = "Team not found"}) pure $ find ((== teamId) . teamTournamentTeam'Dashid) tts

tournamentsTournamentIdQuestionsQuestionGet :: Int -> Int -> Handler [Inline_response_200_3]
tournamentsTournamentIdQuestionsQuestionGet ident question = do
  tts <- liftIO $ tournDB ident
  -- TODO: поправить, чтобы проверял номер вопроса
  let getQuestion (TeamTournament teamId _ (ResultList results)) = Inline_response_200_3 teamId $ resultListInnerValue $ results !! (question-1)
  pure $ map getQuestion tts

updatesGet :: Maybe Integer -> Handler Inline_response_200_4
updatesGet msince = notImplemented >> pure (Inline_response_200_4 0 [])

updatesPost :: UpdateData -> Handler ()
updatesPost input = notImplemented

