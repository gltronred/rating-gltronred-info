module Main where

import ChgkRatingDB.API
import ChgkRatingDB.Backend

config :: ServerConfig
config = ServerConfig
  { configHost = "127.0.0.1"
  , configPort = 1234
  }

main :: IO ()
main = runChgkRatingDBServer config server

